#ifndef SimpleViper_H
#define SimpleViper_H

#include <visp/vpPlot.h>
#include <visp/vpTime.h>

#include <visp/vpRobotViper650.h>


/*!
  \class SimpleViper
  \class that allows to easily configure and pilot the Viper650 or Viper850 in a cartesian frame
*/
class VISP_EXPORT SimpleViper
{
public:
    /*!
    Tool geometries are hard-coded
  */
    typedef enum
    {
        NO_TOOL,
        BIOTAC_TOOL,
    } Tool ;


protected:
    vpRobotViper650 robot;
    vpColVector start_position;
    vpColVector forces_biais;
    vpMatrix lambda;
    double toolMass;
    bool ready;

    vpHomogeneousMatrix tMe; // to express a tool position in the articular frame
    vpHomogeneousMatrix eMt;
    vpForceTwistMatrix sFt; // to express a force measure in the tool frame
    vpForceTwistMatrix tFs; // to express a force setpoin in the sensor frame
    vpVelocityTwistMatrix sVe; // to express a velocity setpoint (computed from force measurements in the sensor frame) in the articular frame
    vpVelocityTwistMatrix tVe; // to express a tool velocity in the articular frame




public:
    SimpleViper(Tool tool);

    // init methods
    void setForceGains (double forceValue, double torqueValue);
    bool calibrateSensor();

    void setStartPosition(float x = 0.5, float y = 0, float z = 0.55, float rx = -M_PI, float ry = 0, float rz = M_PI);
    void goToPosition(vpColVector targetPosition, const double positioningVelocity = 10.0);
    void goToPosition(float x, float y, float z);
    void goToPosition(float x, float y, float z, float rx, float ry, float rz);
    void bringToolToPosition(float x, float y, float z, float rx, float ry, float rz);
    void bringToolToPosition(float x, float y, float z);


protected:
    vpHomogeneousMatrix getMatrixFromVector(vpColVector vector);
    
} ;

#endif

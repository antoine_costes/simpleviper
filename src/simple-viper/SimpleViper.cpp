
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <sys/stat.h>

#include "SimpleViper.h"

/* ---------------------------------------------------------------------- */
/* --- CONSTRUCTOR ------------------------------------------------------ */
/* ---------------------------------------------------------------------- */

SimpleViper::SimpleViper (Tool tool)
    : lambda(6,6), forces_biais(6), start_position(6)
{   
    // Set sensor-to-end-effector geometry
    vpHomogeneousMatrix eMs;
    eMs[2][3] = -0.062; // tz : the end-effector is 6.2cm under the sensor
    vpHomogeneousMatrix sMe;
    eMs.inverse(sMe);

    // Set tool mass and geometry
    switch (tool)
    {
    case NO_TOOL:
        tMe = vpHomogeneousMatrix();
        toolMass = 0;
        break;

    case BIOTAC_TOOL:

        tMe = vpHomogeneousMatrix(vpTranslationVector(0.1, 0, -0.2), vpRotationMatrix(0, 0, 0));

        toolMass = 0.1;
        std::cout << "[SimpleViper] Biotac tool geometry : " <<std::endl;
        tMe.print();
        std::cout << "" << std::endl;
        std::cout << "[SimpleViper] Biotac tool mass : " << toolMass << std::endl;
        break;

    }
    tMe.inverse(eMt);
    robot.set_eMc(eMt); // camera and tool are the same thing here

    // sensor to tool geometry
    vpHomogeneousMatrix sMt;
    vpHomogeneousMatrix tMs;
    sMt = sMe*eMt;
    sMt.inverse(tMs);


    // In force control, the velocities must be computed from the measured forces.
    // So we'll want to express our tool force setpoint into the sensor frame for computing the command.
    tFs.buildFrom(tMs); // get force matrix from tool to sensor frame

    // Then, we'll get a velocity command in the sensor frame so we'll want to express it in the end-effector frame to pilot the robot.
    sVe.buildFrom(sMe); // get velocity matrix from sensor to end-effector frame

    // We'll also want to express our tool velocity setpoint in the end-effector frame to pilot the robot.
    tVe.buildFrom(tMe);

    // In contact detection, we'll want to deduce forces applied to the tool from measured forces in the sensor frame
    sFt.buildFrom(sMt); // get force matrix from sensor to tool frame

    // init robot
    setForceGains(0.01, 0.1);
    robot.setMaxTranslationVelocity(0.5);
    robot.setMaxRotationVelocity(vpMath::rad(180));
    setStartPosition();

    ready = calibrateSensor();
    if (!ready)
    {
        std::cout << "****************" << std::endl;
        std::cout << "* SENSOR ERROR *" << std::endl;
        std::cout << "****************" << std::endl;
    }
}

// configure and get values
void
SimpleViper::setForceGains (double forceValue, double torqueValue)
{
    for (int i=0; i< 3; i++)
        lambda[i][i] = forceValue;

    for (int i=3; i< 6; i++)
        lambda[i][i] = torqueValue;
}

void
SimpleViper::setStartPosition(float x, float y, float z, float rx, float ry, float rz)
{
    // x, y, z in meter
    // rx, ry, rz in rad
    start_position = vpColVector(vpPoseVector(x,y,z, rx, ry, rz));
}

void
SimpleViper::goToPosition(vpColVector targetPosition, const double positioningVelocity)
{
    // go to position
}


void
SimpleViper::goToPosition(float x, float y, float z, float rx, float ry, float rz)
{
    // x, y, z in meter
    // rx, ry, rz in rad
    goToPosition(vpColVector(vpPoseVector(x, y, z, rx, ry, rz)));
}

void
SimpleViper::goToPosition(float x, float y, float z)
{
    goToPosition(x, y, z, 0, 0, 0);
}

void
SimpleViper::bringToolToPosition(float x, float y, float z, float rx, float ry, float rz)
{
    // bring tool to position
}

void
SimpleViper::bringToolToPosition(float x, float y, float z)
{
    bringToolToPosition(x, y, z, 0, 0, 0);
}


bool
SimpleViper::calibrateSensor()
{
    try
    {
        robot.biasForceTorqueSensor() ;
    } catch(vpRobotException exception)
    {
        std::cout << "" << std::endl;
        return false;
    }

    // take tool weight in account ?
    robot.getForceTorque(forces_biais);
    forces_biais *= -1;
    //forces_biais -= sFg * gHg;

    std::cout << "[SimpleViper] Force sensor calibrated." << std::endl;
    return true;
}


vpHomogeneousMatrix
SimpleViper::getMatrixFromVector(vpColVector position)
{
    vpTranslationVector txyz;
    vpRxyzVector rxyz;
    for (unsigned int i=0; i < 3; i++)
    {
        txyz[i] = position[i];
        rxyz[i] = position[i+3];
    }
    vpRotationMatrix rotM(rxyz);
    vpHomogeneousMatrix M(txyz, rotM);
    return M;
}

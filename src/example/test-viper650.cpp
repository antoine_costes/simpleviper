#include <visp3/robot/vpRobotViper650.h>

int main()
{
  //********* Define transformation from end effector to tool frame
  vpHomogeneousMatrix eMt;

  eMt[0][0] = 0;
  eMt[1][0] = 0;
  eMt[2][0] = -1;

  eMt[0][1] = -sqrt(2)/2;
  eMt[1][1] = -sqrt(2)/2;
  eMt[2][1] = 0;

  eMt[0][2] = -sqrt(2)/2;
  eMt[1][2] = sqrt(2)/2;
  eMt[2][2] = 0;

  eMt[0][3] = -0.177;
  eMt[1][3] = 0.177;
  eMt[2][3] = 0.077;

  std::cout << "eMt:\n" << eMt << std::endl;

  //********* Init robot
  vpRobotViper650 robot;
  robot.init(vpViper650::TOOL_CUSTOM, eMt);
  robot.set_eMc(eMt);

  vpColVector q;
  robot.getPosition(vpRobotViper650::ARTICULAR_FRAME, q);

  vpHomogeneousMatrix fMe, fMt;

  robot.get_fMe(q, fMe);
  robot.get_fMc(q, fMt);

  std::cout << "fMe:\n" << fMe << std::endl;
  std::cout << "fMt:\n" << fMt << std::endl;

  //********* Check if retrieved eMt transformation is the one that was set

  vpHomogeneousMatrix eMt_ = fMe.inverse() * fMt;
  std::cout << "eMt_:\n" << eMt_ << std::endl;

  //********* Test inverse kinematics
  robot.getInverseKinematics(fMt, q);

  std::cout << "Move robot in joint (the robot should not move)" << std::endl;
  robot.setRobotState(vpRobot::STATE_POSITION_CONTROL);
  robot.setPosition(vpRobotViper650::ARTICULAR_FRAME, q);

  //********* Position control in tool frame

  // from the current position move the tool frame
  vpHomogeneousMatrix tMt;
  //tMt[0][3] = 0.05; // along x_t
  tMt[1][3] = 0.05; // along y_t
//  tMt[2][3] = 0.05; // along z_t

  robot.getInverseKinematics(fMt*tMt, q);

  std::cout << "Move robot in joint position" << std::endl;
  robot.setRobotState(vpRobot::STATE_POSITION_CONTROL);
  robot.setPosition(vpRobotViper650::ARTICULAR_FRAME, q);

  //********* Velocity control in tool frame
  if (1){
    vpVelocityTwistMatrix tVe(eMt.inverse());
    vpMatrix eJe;

    double t_init = vpTime::measureTimeMs();
    vpColVector v_t(6), q_dot;
    v_t = 0;
    v_t[2] = 0.01; // translation velocity along z_t

    std::cout << "Move robot in joint velocity" << std::endl;
    robot.setRobotState(vpRobot::STATE_VELOCITY_CONTROL);
    while(vpTime::measureTimeMs() - t_init < 3000) {
      robot.get_eJe(eJe);
      vpMatrix tJt = tVe * eJe;
      q_dot = tJt.pseudoInverse() * v_t;
      //std::cout << "send vel: " << q_dot.t() << std::endl;
      robot.setVelocity(vpRobotViper650::ARTICULAR_FRAME, q_dot);
    }
  }

  //********* Position control in tool frame

  // get current position
  robot.getPosition(vpRobotViper650::ARTICULAR_FRAME, q);
  robot.get_fMc(q, fMt);

  tMt.eye();
  //tMt[0][3] = -0.05; // along x_t
  tMt[1][3] = -0.05; // along y_t
//  tMt[2][3] = -0.05; // along z_t

  robot.getInverseKinematics(fMt*tMt, q);

  std::cout << "Move robot in joint position" << std::endl;
  robot.setRobotState(vpRobot::STATE_STOP);
  robot.setRobotState(vpRobot::STATE_POSITION_CONTROL);
  robot.setPosition(vpRobotViper650::ARTICULAR_FRAME, q);


  std::cout << "The end" << std::endl;
}


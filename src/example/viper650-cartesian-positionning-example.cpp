#include <visp3/robot/vpRobotViper650.h>

int main()
{

    std::cout << "Test Viper650 cartesian positionning" << std::endl;

    // init the robot without specific tool
    vpRobotViper650 robot;
    vpHomogeneousMatrix eMt;
    eMt = vpHomogeneousMatrix(vpTranslationVector(0.1, 0, -0.2), vpRotationMatrix(0, 0, 0));

    robot.init(vpViper650::TOOL_CUSTOM, eMt);

    // define some cartesian positions
    vpColVector targetCartesianPosition1 = vpColVector(vpPoseVector(0.5, 0, 0.6, M_PI, 0, -M_PI/4));
    vpColVector targetCartesianPosition2 = vpColVector(vpPoseVector(0.5, 0, 0.65, M_PI, 0, -M_PI/4));
    vpColVector targetCartesianPosition3 = vpColVector(vpPoseVector(0.5, 0, 0.65, M_PI, 0, -M_PI/2));


    std::cout << "Cartesian positionning using reference frame" << std::endl;

    robot.setRobotState(vpRobot::STATE_POSITION_CONTROL);

    robot.setPosition(vpRobotViper650::REFERENCE_FRAME, targetCartesianPosition1);

    robot.setPosition(vpRobotViper650::REFERENCE_FRAME, targetCartesianPosition2);

    //robot.setPosition(vpRobotViper650::REFERENCE_FRAME, targetCartesianPosition3);


    vpColVector q(6);
    vpHomogeneousMatrix fMt;
    robot.getPosition(vpRobotViper650::ARTICULAR_FRAME, q);
    vpHomogeneousMatrix toolCartesianPosition = robot.getForwardKinematics(q);
    robot.get_fMc(q, fMt); // c'est la même chose


    std::cout << "Articular pos" << q.t() << std::endl;
    std::cout << "fMt" << std::endl;
    fMt.print();
    std::cout << "" << std::endl;
    std::cout << "Tool cartesian pos" << std::endl;
    toolCartesianPosition.print();
    std::cout << "" << std::endl;

    // Compute corresponding articular positions using current articular position

    vpTranslationVector txyz;
    vpRxyzVector rxyz;
    for (unsigned int i=0; i < 3; i++)
    {
        txyz[i] = targetCartesianPosition1[i];
        rxyz[i] = targetCartesianPosition1[i+3];
    }
    vpRotationMatrix rotM(rxyz);
    vpHomogeneousMatrix targetMatrix1(txyz, rotM);

    robot.getPosition(vpRobotViper650::ARTICULAR_FRAME, q);
    robot.getInverseKinematics(targetMatrix1, q);
    robot.setPosition(vpRobotViper650::ARTICULAR_FRAME, q);
/*
    vpHomogeneousMatrix targetArticularPosition2;
    robot.getPosition(vpRobotViper650::ARTICULAR_FRAME, q);
    robot.getInverseKinematics(targetArticularPosition2, q);
    robot.setPosition(vpRobotViper650::ARTICULAR_FRAME, targetArticularPosition2);

    vpHomogeneousMatrix targetArticularPosition3;
    robot.getPosition(vpRobotViper650::ARTICULAR_FRAME, q);
    robot.getInverseKinematics(targetArticularPosition3, q);
    robot.setPosition(vpRobotViper650::ARTICULAR_FRAME, targetArticularPosition3);
*/
    std::cout << "The end" << std::endl;
}


#include <visp3/robot/vpRobotViper650.h>

int main()
{
    bool printStartPosition = false;
    bool goToOtherPosition = false;
    bool testDirections = false;
    bool goToContact = true;

    std::cout << "" << std::endl;

    /* ---------------------------------------------------------------------- */
    /* --- GEOMETRY --------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    //********* Define transformation from end effector to tool frame
    vpHomogeneousMatrix eMt;
    double l = 0.18; // finger length
    double h = 0.015; // finger heigth offset
    double angle = -M_PI/4;
    vpTranslationVector tip_position(-cos(angle)*l, cos(angle)*l, h);
    eMt = vpHomogeneousMatrix(tip_position, vpRotationMatrix(0, 0, angle));

    eMt.eye();

    //********* Define high level tool directions : y is forwards, z is downwards
    vpColVector tool_left(vpPoseVector(1, 0, 0, 0, 0, 0));
    vpColVector tool_right(vpPoseVector(-1, 0, 0, 0, 0, 0));
    vpColVector tool_forward(vpPoseVector(0, 1, 0, 0, 0, 0));
    vpColVector tool_backward(vpPoseVector(0, -1, 0, 0, 0, 0));
    vpColVector tool_down(vpPoseVector(0, 0, 1, 0, 0, 0));
    vpColVector tool_up(vpPoseVector(0, 0, -1, 0, 0, 0));
    vpColVector zero(6);


    //********* Define transformation from end effector to sensor frame
    vpHomogeneousMatrix eMs;
    eMs[2][3] = -0.062; // tz : the sensors is 6.2cm above the sensor and z is towards down
    vpHomogeneousMatrix sMe;
    eMs.inverse(sMe);

    vpHomogeneousMatrix sMt(sMe*eMt);
    vpHomogeneousMatrix tMs(sMt.inverse());

    std::cout << "sMt:\n" << sMt << std::endl;

    /* ---------------------------------------------------------------------- */
    /* --- INIT ROBOT ------------------------------------------------------- */
    /* ---------------------------------------------------------------------- */

    vpRobotViper650 robot;
    robot.init(vpViper650::TOOL_CUSTOM, eMt);
    //robot.set_eMc(eMt);
    vpMatrix lambda(6,6);
    for (int i=0; i< 3; i++)lambda[i][i] = 0.01; // Initialized the force gain
    for (int i=3; i< 6; i++) lambda[i][i] = 0.1; // Initialized the torque gain
    vpColVector q(6);


    /* ---------------------------------------------------------------------- */
    /* --- POSITIONNING ----------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    vpColVector startPosition(vpPoseVector(0.5, -0.2, 0.4, 0, -M_PI, M_PI/2));
    //startPosition = vpColVector(vpPoseVector(0.4, -0.2, 0.4, 0, -M_PI, M_PI/2)); // cette position ne marche pas ?

    vpHomogeneousMatrix startPosM(vpTranslationVector(startPosition[0], startPosition[1], startPosition[2]),
            vpRotationMatrix(vpRxyzVector(startPosition[3], startPosition[4], startPosition[5])));
    //********* Get corresponding articular position
    vpColVector q_start(6);
    robot.getPosition(vpRobotViper650::ARTICULAR_FRAME, q);
    robot.getInverseKinematics(startPosM, q);
    q_start = q;

    std::cout << "" << std::endl;
    std::cout << "**********************************" << std::endl;
    std::cout << "***** Move to start position *****" << std::endl;
    std::cout << "**********************************" << std::endl;
    std::cout << "" << std::endl;

    if (printStartPosition)
    {
        vpColVector q_start_deg(q_start);
        q_start_deg.rad2deg();
        std::cout << q_start.t() << std::endl;
        std::cout << q_start_deg.t() << std::endl;
    }

    robot.setRobotState(vpRobot::STATE_POSITION_CONTROL);
    robot.setPosition(vpRobotViper650::ARTICULAR_FRAME, q_start);

    if (goToOtherPosition)
    {
        vpColVector otherPosition(vpPoseVector(0.5, -0.2, 0.4, 0, -M_PI, M_PI/4));

        vpHomogeneousMatrix otherPosM(vpTranslationVector(otherPosition[0], otherPosition[1], otherPosition[2]),
                vpRotationMatrix(vpRxyzVector(otherPosition[3], otherPosition[4], otherPosition[5])));
        //********* Get corresponding articular position
        vpColVector q_target(6);
        robot.getPosition(vpRobotViper650::ARTICULAR_FRAME, q);
        robot.getInverseKinematics(otherPosM, q);
        q_target = q;

        std::cout << "" << std::endl;
        std::cout << "**********************************" << std::endl;
        std::cout << "***** Move to other position *****" << std::endl;
        std::cout << "**********************************" << std::endl;
        std::cout << "" << std::endl;

        robot.setRobotState(vpRobot::STATE_POSITION_CONTROL);
        robot.setPosition(vpRobotViper650::ARTICULAR_FRAME, q_target);
    }

    /* ---------------------------------------------------------------------- */
    /* --- VELOCITY CONTROL ------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    vpColVector vel_t;
    vpColVector q_dot_t(6);
    vpVelocityTwistMatrix tVe(eMt.inverse());
    double t_init = vpTime::measureTimeMs();
    double time = 0;
    double elapsed_time = 0;

    //********* Move at constant speed according to high level directions
    if (testDirections)
    {
        std::cout << "" << std::endl;
        std::cout << "*************************************" << std::endl;
        std::cout << "* Velocity control, test directions *" << std::endl;
        std::cout << "*************************************" << std::endl;
        std::cout << "" << std::endl;

        robot.setRobotState(vpRobot::STATE_VELOCITY_CONTROL);
        while(elapsed_time < 4000)
        {
            time = vpTime::measureTimeMs();

            //********* Test one direction per second
            vel_t = tool_left*0.01;
            if (time - t_init > 1000) vel_t = tool_right*0.01;
            if (elapsed_time > 2000) vel_t = tool_forward*0.01;
            if (elapsed_time > 3000) vel_t = tool_down*0.01;

            //********* Compute velocity in the tool frame
            vpMatrix eJe;
            robot.get_eJe(eJe); // get robot jacobian
            vpMatrix tJt = tVe.inverse() * eJe; // express it in the tool frame
            q_dot_t = tJt.pseudoInverse() * vel_t;  // compute articular velocities

            robot.setVelocity(vpRobot::ARTICULAR_FRAME, q_dot_t);
            elapsed_time = time - t_init;
            elapsed_time = vpTime::measureTimeMs() - t_init;
        }
    }

    /* ---------------------------------------------------------------------- */
    /* --- FORCE CONTROL ---------------------------------------------------- */
    /* ---------------------------------------------------------------------- */
    vpColVector forces_t;
    vpColVector v_command_s(6);
    vpColVector v_command_t(6);
    vpForceTwistMatrix sFt(sMt);
    vpVelocityTwistMatrix tVs(tMs);
    vpColVector q_dot_forces_t(6);

    //********* Apply vertical force until a reaction is measured
    if (goToContact)
    {
        std::cout << "" << std::endl;
        std::cout << "***********************" << std::endl;
        std::cout << "***** Bias sensor *****" << std::endl;
        std::cout << "***********************" << std::endl;
        std::cout << "" << std::endl;

        std::cout << "Bias sensor" << std::endl;
        robot.biasForceTorqueSensor();
        vpColVector bias_s;
        robot.getForceTorque(bias_s);
        std::cout << "Measure bias : " << bias_s.t() << std::endl;

        std::cout << "" << std::endl;
        std::cout << "*************************" << std::endl;
        std::cout << "***** Go to contact *****" << std::endl;
        std::cout << "*************************" << std::endl;
        std::cout << "" << std::endl;

        robot.setRobotState(vpRobot::STATE_VELOCITY_CONTROL);

        t_init = vpTime::measureTimeMs();
        time = 0;
        elapsed_time = 0;
        while(elapsed_time < 5000)
        {
            time = vpTime::measureTimeMs();

            //********* Wait a second then apply the force
            forces_t = zero;
            if (elapsed_time > 1000)
            {
                forces_t = tool_down*0.1;
            }

            // *** Compute velocity command in the sensor frame for better accuracy
            // forces are measured in the sensor frame
            vpColVector forcetorque_s;
            robot.getForceTorque(forcetorque_s);
            //forcetorque_s -= bias;
            forcetorque_s *= -1; // get forces applied by the robot on the environment

            v_command_s = lambda*(sFt * forces_t - forcetorque_s - bias_s); // the command is a simple proportionnal gain

            //********* Compute articular velocity command in the tool frame
            v_command_t = tVs * v_command_s;  // express velocity command in the tool frame
            vpMatrix eJe;
            robot.get_eJe(eJe); // get robot jacobian
            vpMatrix tJt = tVe.inverse() * eJe; // express it in the tool frame
            q_dot_forces_t = tJt.pseudoInverse() * v_command_t;  // compute articular velocities

            robot.setVelocity(vpRobot::ARTICULAR_FRAME, q_dot_forces_t);
            elapsed_time = time - t_init;
            vpTime::wait(time, 20);
        }
    }


    std::cout << "The end" << std::endl;
}

